package com.enthu.ble;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.enthu.ble.activity.BrowserActivity;
import com.enthu.ble.activity.DeviceServicesActivity;
import com.enthu.ble.ble.BlueToothService;
import com.enthu.ble.ble.TimeoutGattCallback;
import com.enthu.ble.bluetoothdatamodel.datatypes.Characteristic;
import com.enthu.ble.bluetoothdatamodel.datatypes.Descriptor;
import com.enthu.ble.bluetoothdatamodel.datatypes.Service;
import com.enthu.ble.bluetoothdatamodel.parsing.Common;
import com.enthu.ble.bluetoothdatamodel.parsing.Converters;
import com.enthu.ble.bluetoothdatamodel.parsing.Device;
import com.enthu.ble.bluetoothdatamodel.parsing.Engine;
import com.enthu.ble.dialogs.ErrorDialog;
import com.enthu.ble.log.TimeoutLog;
import com.enthu.ble.services.BluetoothLeService;
import com.enthu.ble.services.Write;
import com.enthu.ble.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_CONNECTED;
import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR;
import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_DISCONNECTED;

public class Dashboard extends AppCompatActivity {
    private BluetoothGattCharacteristic mBluetoothCharact;
    public static UUID ota_service = UUID.fromString("1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0");
    private UUID ota_control = UUID.fromString("f7bf3564-fb6d-4e53-88a4-5e37e0326063");
    
    private BlueToothService.Binding bluetoothBinding;
    private BluetoothAdapter mBluetoothAdapter;
    private BlueToothService service;
    TextView dataSMS, dataBattery, textbt, sms_value,smstext,battery_value;
    CardView sms, battery;
    Integer data1, data2,j=0;
    Integer blue,Green,yellow, red;
    DeviceServicesActivity deviceServicesActivity;
    BluetoothGattCharacteristic soilMoistureLevel,batteryLevel;
    BluetoothGattService mGattService;
    BluetoothLeService blueToothService;
    BluetoothGatt bluetoothGatt;
    Button Refresh;
    private String deviceAddress;
    Handler handler;
    private boolean serviceHasBeenSet;
    Integer SMSVALUE;

    public void click(View v) {

        Intent i = new Intent();
        i.setClass(this, Dropdown.class);
        startActivity(i);
    }


    int i=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        data1 = 1000;
        data2 = 3100;
        dataSMS = (TextView) findViewById(R.id.sms_value);
        dataBattery = findViewById(R.id.battery_value);
        smstext = findViewById(R.id.smstext);
        textbt = findViewById(R.id.textbt);
        sms = findViewById(R.id.sms);
        battery = findViewById(R.id.battery);



        initDevice(getDeviceAddress(savedInstanceState));


}

    public void readdata() {


        Thread t = new Thread() {
            int i = 0;

            @Override
            public void run() {

//                        bluetoothGatt.readCharacteristic(batteryLevel);
                while (!isInterrupted()) {

                    try {

                        Thread.sleep(2000);

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {


                                if (i <= 15) {
                                    bluetoothGatt.readCharacteristic(soilMoistureLevel);
                                    i++;
                                    j = i;

                                }

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }

        };

        t.start();

        Thread t2 = new Thread() {


            @Override
            public void run() {
//                        bluetoothGatt.readCharacteristic(batteryLevel);
                while (!isInterrupted()) {

                    try {
                        Thread.sleep(2000);  //1000ms = 1 sec

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                bluetoothGatt.readCharacteristic(batteryLevel);

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        t2.start();
    }
    private  void  ColorConfiguration(){
        if(Write.colorTypes=="SANDY"){
            Sandy(SMSVALUE);
        }
        else if (Write.colorTypes=="CLAY"){
            Clay(SMSVALUE);
        }
        else if(Write.colorTypes=="SILT"){

            Silt(SMSVALUE);
        }
        else{
            colorSmsAction (SMSVALUE);
        }

    }
    private void Sandy (int data){
        if (246 >= data) {
            sms.setCardBackgroundColor(Color.parseColor("#00ccff")); //Blue
        }
        else if(614 >= data){
            sms.setCardBackgroundColor(Color.parseColor("#32CD32")); //Green
            }
        else if(1229 >= data){

            sms.setCardBackgroundColor(Color.parseColor("#ffff66")); // Yellow
            dataSMS.setTextColor(Color.BLACK);
            smstext.setTextColor(Color.BLACK);
            }
        else {
            sms.setCardBackgroundColor(Color.parseColor("#ff3333")); //Red
        }


    }
    private void Clay (int data){
        if (491 >= data) {
            sms.setCardBackgroundColor(Color.parseColor("#00ccff"));  //Blue
        }
        else if(983 >= data){
            sms.setCardBackgroundColor(Color.parseColor("#32CD32")); //Green
        }
        else if(1638 >= data){

            sms.setCardBackgroundColor(Color.parseColor("#ffff66"));//Yellow
            dataSMS.setTextColor(Color.BLACK);
            smstext.setTextColor(Color.BLACK);
        }
        else {
            sms.setCardBackgroundColor(Color.parseColor("#ff3333")); // Red
        }


    }
    private void Silt (int data){
        if (1024 >= data) {
            sms.setCardBackgroundColor(Color.parseColor("#00ccff")); // Blue
        }
        else if(1638 >= data){
            sms.setCardBackgroundColor(Color.parseColor("#32CD32"));// green
        }
        else if(2048 >= data){

            sms.setCardBackgroundColor(Color.parseColor("#ffff66")); //yellow
            dataSMS.setTextColor(Color.BLACK);
            smstext.setTextColor(Color.BLACK);
        }
        else {
            sms.setCardBackgroundColor(Color.parseColor("#ff3333")); // red
        }


    }
    private void colorSmsAction(int data1) {
        if(246<=data1){

        }



        if (data1 <= 1000) {
            sms.setCardBackgroundColor(Color.parseColor("#00ccff"));
        } else if (data1 <= 2500) {
            sms.setCardBackgroundColor(Color.parseColor("#32CD32"));
        } else if (data1 <= 3500) {

            sms.setCardBackgroundColor(Color.parseColor("#ffff66"));
            dataSMS.setTextColor(Color.BLACK);
            smstext.setTextColor(Color.BLACK);




        }else {

            sms.setCardBackgroundColor(Color.parseColor("#ff3333"));
        }
    }
    public void colorbatteryAction(int data2){
        dataBattery.setText(Integer.toString(data2));
        /// battery value color change
        Integer MinimumValue = 3100;
        Integer MaximumValue = 3300;
        Integer difference = MaximumValue - MinimumValue;
        Integer batteryPercentage =
                ((data2 - MinimumValue) / difference) * 100;
//        dataBattery.setText(batteryPercentage);
        if (data2 < 3350) {
            battery.setCardBackgroundColor(Color.parseColor("#ff3333"));
        } else if (data2 < 3450) {
            battery.setCardBackgroundColor(Color.parseColor("#ffff66"));
            dataBattery.setTextColor(Color.BLACK);
            textbt.setTextColor(Color.BLACK);
        } else {
            battery.setCardBackgroundColor(Color.parseColor("#32CD32"));
        }

    }

//    @Override
//    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//
////        if (currentWriteReadFragment != null) {
////                currentWriteReadFragment.onActionDataAvailable(characteristic.getUuid().toString());
//
//
//    }
    // getting the device Address through the previous activity Selected Device
    private String getDeviceAddress(final Bundle savedInstanceState) {
        String deviceAddress;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            System.out.println(extras);
            if (extras == null) {
                deviceAddress = null;
            } else {
                deviceAddress = extras.getString("DEVICE_SELECTED_ADDRESS");
            }
        } else {
            deviceAddress = savedInstanceState.getString("DEVICE_SELECTED_ADDRESS");
        }
        this.deviceAddress = deviceAddress;
        System.out.println(mBluetoothAdapter.getRemoteDevice(deviceAddress));
        initDevice(deviceAddress);
   return  deviceAddress;

    }


    ///====================================================================
    //======================Load characterstics =================================
    private void loadCharacteristicDescriptors(BluetoothGattCharacteristic bluetoothGattCharacteristic)
    {
        for (BluetoothGattDescriptor d : bluetoothGattCharacteristic.getDescriptors()) {
            Descriptor descriptor = Engine.getInstance().getDescriptorByUUID(d.getUuid());


            if (descriptor == null) {

            } else {
                descriptor.getName();
                descriptor.getName();
            }


        }
    }


    //===================================================================





    //===============================================================================
    //=====================INIT DEVICE ==============================================
    private void initServicesViews() {

        // iterate through all of the services for the device, inflate and add views to the scrollview
        ArrayList<BluetoothGattService> services = (ArrayList<BluetoothGattService>) bluetoothGatt.getServices(); //service.getConnectedGatt().getServices();
        for (int position = 0; position < services.size(); position++) {


            // get information about service at index 'position'
            UUID uuid = services.get(position).getUuid();
            Service service = Engine.getInstance().getService(uuid);
            String serviceName = Common.getServiceName(uuid, getApplicationContext());
             String serviceUuid = Common.getUuidText(uuid);

            serviceName = Common.checkOTAService(serviceUuid, serviceName);

            // initialize information about services in service item container
            System.out.println(serviceUuid);


            // initialize views for each characteristic of the service, put into characteristics expansion for service's list item
            final BluetoothGattService blueToothGattService = service == null ? services.get(position) : bluetoothGatt.getService(service.getUuid());
            List<BluetoothGattCharacteristic> characteristics = blueToothGattService.getCharacteristics();
            if (characteristics.size() == 0) {

                continue;
            }
            // iterate through the characteristics of this service
            for (final BluetoothGattCharacteristic bluetoothGattCharacteristic : characteristics) {
                // retrieve relevant bluetooth data for characteristic of service
                final BluetoothGattCharacteristic thisCharacteristic = bluetoothGattCharacteristic;
                // the engine parses through the data of the btgattcharac and returns a wrapper characteristic
                // the wrapper characteristic is matched with accepted bt gatt profiles, provides field types/values/units
                System.out.println(bluetoothGattCharacteristic.getUuid().toString());

                Characteristic charact = Engine.getInstance().getCharacteristic(bluetoothGattCharacteristic.getUuid());
                String characteristicName;

                if (charact != null) {
                    characteristicName = charact.getName().trim();
                } else {
                    characteristicName =bluetoothGattCharacteristic.getUuid().toString();
                }


                final String characteristicUuid = (charact != null ? Common.getUuidText(charact.getUuid()) : Common.getUuidText(bluetoothGattCharacteristic.getUuid()));

                //TODO: They are in GattCharacteristic, but their names are not appearing




                loadCharacteristicDescriptors(bluetoothGattCharacteristic);

                // init/populate ui elements with info from bluetooth data for characteristic of service

                System.out.println(characteristicName);
                if("00001a00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    soilMoistureLevel =bluetoothGattCharacteristic;

                    System.out.println( bluetoothGattCharacteristic.getValue());
                }
                if("00003a00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    batteryLevel=bluetoothGattCharacteristic;
                }
                readdata();
                if("00001b00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    Write.smsDryThershold=bluetoothGattCharacteristic;
                }
                if("00001c00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                Write.smsWetThershold=bluetoothGattCharacteristic;
                }
                if("00001d00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    Write.smsMediumThershold=bluetoothGattCharacteristic;
                }
                if("00002b00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    Write.batHighThershold=bluetoothGattCharacteristic;
                }
                if("00002c00-0000-1000-8000-00805f9b34fb".equals(bluetoothGattCharacteristic.getUuid().toString())){
                    Write.batLowThershold=bluetoothGattCharacteristic;
                }


System.out.println(characteristicUuid);



            }
        }
    }

    //===============================================================================



    private void initDevice(final String deviceAddress) {
        handler = new Handler();

        bluetoothBinding = new BlueToothService.Binding(this) {
            @Override
            protected void onBound(BlueToothService service) {//todo dubel
                serviceHasBeenSet = true;
final BluetoothDevice device =mBluetoothAdapter.getRemoteDevice(deviceAddress);
                Dashboard.this.service = service;
                service.connectGatt(device,false,gattCallback);
                if (!service.isGattConnected(deviceAddress)) {
                    Toast.makeText(Dashboard.this, R.string.toast_debug_connection_failed, Toast.LENGTH_LONG).show();

                } else {
                    BluetoothGatt bG = service.getConnectedGatt(deviceAddress);
                    System.out.println(bG.getDevice());

                    if (bG == null) {
                        Toast.makeText(Dashboard.this, R.string.device_not_from_EFR, Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                    service.registerGattCallback(true, gattCallback);
                    System.out.println(bG.getServices());
                    System.out.println(bG.getService(UUID.fromString("00001A00-0000-1000-8000-00805F9B34FB")));
                    if (bG.getServices() != null && !bG.getServices().isEmpty()) {
                        bluetoothGatt = bG;
                        System.out.println(bluetoothGatt);
                        initServicesViews();

                    } else {

                        bG.discoverServices();
                        Write.gattService=bG;


                    }
                }
            }
        };
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BlueToothService.bind(bluetoothBinding);
                    }
                });
            }
        }, 0);

    }

    /**
     * BLUETOOTH GATT CALLBACKS
     *********************************************************/
    private TimeoutGattCallback gattCallback = new TimeoutGattCallback() {
        @Override
        public void onReadRemoteRssi(final BluetoothGatt gatt, final int rssi, int status) {

                super.onReadRemoteRssi(gatt, rssi, status);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("onReadRemoteRssi", "RSSI: " + rssi);

                    }
                });

        }

        @Override
        public void onTimeout() {
            Constants.LOGS.add(new TimeoutLog());
            super.onTimeout();
            Log.d("gattCallback", "onTimeout");
        }


//OnConnectionState Change
// It can be either connected or disconnected state
@Override
public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
    super.onConnectionStateChange(gatt, status, newState);
    runOnUiThread(new Runnable() {
        @Override
        public void run() {
//            Toast.makeText(Dashboard.this,"Device timeout",Toast.LENGTH_LONG).show();
            Intent i= new Intent(Dashboard.this,BrowserActivity.class);
            startActivity(i);
        }
    });
    Log.d("onConnectionStateChange", "status = " + status + " - newState = " + newState);

    Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);




}


        @Override //CALLBACK ON CHARACTERISTIC READ
        public void onCharacteristicRead(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if("00001a00-0000-1000-8000-00805f9b34fb".equals(characteristic.getUuid().toString())){
                data1= Integer.valueOf(Converters.getDecimalValue1(characteristic.getValue()));
                SMSVALUE=data1;
                System.out.println(data1);
            dataSMS.setText(Integer.toString(data1));
//                colorSmsAction(data1);
                ColorConfiguration();
            }

            if("00003a00-0000-1000-8000-00805f9b34fb".equals(characteristic.getUuid().toString())){
                data2=Integer.valueOf(Converters.getDecimalValue1(characteristic.getValue()));

                colorbatteryAction(data2);
            }



            }


        @Override //CALLBACK ON CHARACTERISTIC WRITE (PROPERTY: WHITE)
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {


            if (characteristic.getValue().length < 10)

                Log.d("OnCharacteristicRead", "Char: " + characteristic.getUuid().toString() + " Value: " + Converters.getHexValue(characteristic.getValue()) + " Status: " + status);

            if (status != 0) { // Error Handling
                Log.d("onCharWrite", "status: " + Integer.toHexString(status));
                final int error = status;
            }

            Toast.makeText(getApplicationContext(),"Updated Successfully",Toast.LENGTH_LONG).show();



           /// bluetoothGatt.readCharacteristic(characteristic);
        }

        @Override //CALLBACK ON DESCRIPTOR WRITE
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
//            if (currentWriteReadFragment != null) {
//                currentWriteReadFragment.onDescriptorWrite(descriptor.getUuid());
//            }
        }

        @Override //CALLBACK ON DESCRIPTOR READ
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {



                byte[] value = new byte[2];
                value[0] = (byte) 0xF2;
                value[1] = (byte) 0xFF;

                if (descriptor.getValue()[0] == value[0] && descriptor.getValue()[1] == value[1]) {

                    Log.i("descriptor", "getValue " + Converters.getHexValue(descriptor.getValue()));


                }
            }


        @Override //CALLBACK ON CHARACTERISTIC CHANGED VALUE (READ - CHARACTERISTIC NOTIFICATION)
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

//            for (int key : characteristicFragments.keySet()) {
//                FragmentCharacteristicDetail fragment = characteristicFragments.get(key);
//                if (fragment != null && fragment.getmCharact().getUuid().equals(characteristic.getUuid())) {
//                    fragment.onActionDataAvailable(characteristic.getUuid().toString());
//                    break;
//                }
//            }

        }

        @Override //CALLBACK ON SERVICES DISCOVERED
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
//            bluetoothGatt = gatt;

            if (bluetoothGatt != gatt) {
                bluetoothGatt = gatt;

            }
            initServicesViews();
            getServicesInfo(gatt);

        }
    };
    /************************************************************************************/


    public BluetoothGatt getBluetoothGatt() {
        return bluetoothGatt;
    }


    public void getServicesInfo(BluetoothGatt gatt) {

        List<BluetoothGattService> gattServices = gatt.getServices();
        Log.i("onServicesDiscovered", "Services count: " + gattServices.size());

        for (BluetoothGattService gattService : gattServices) {
            String serviceUUID = gattService.getUuid().toString();


            Log.i("onServicesDiscovered", "Service UUID " + serviceUUID + " - Char count: " + gattService.getCharacteristics().size());
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();

            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

                String CharacteristicUUID = gattCharacteristic.getUuid().toString();
                Log.i("onServicesDiscovered", "Characteristic UUID " + CharacteristicUUID + " - Properties: " + gattCharacteristic.getProperties());


            }
        }
    }
}












