package com.enthu.ble;


import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.enthu.ble.activity.BrowserActivity;
import com.enthu.ble.activity.MainMenuActivity;
import com.enthu.ble.bluetoothdatamodel.parsing.Device;
import com.enthu.ble.bluetoothdatamodel.parsing.Engine;
import com.enthu.ble.services.Write;


import java.util.Arrays;

import butterknife.InjectView;

import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_CONNECTED;
import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR;
import static com.enthu.ble.services.BluetoothLeService.ACTION_GATT_DISCONNECTED;


public class Silt extends AppCompatActivity {
    EditText text,batLowtextsilt,textHighsilt,textSmsDrysilt,textSmsWetsilt,textSmsMediumsilt;
    Button btn,battLowsilt,bttnHighsilt,bttnsmsWetsilt,bttsmsDrysilt,bttsmsMediumsilt,btnhelp;
    BluetoothGatt magattService;
    @InjectView(R.id.help_button)
    TextView helpButton;

    private Dialog helpDialog;
    private Dialog hiddenDebugDialog;
    private String DEVICE_ADDRESS;
    private Dialog alertDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_silt);

        findViewById(R.id.buttonShowDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //calling this method to show our android custom alert dialog
                showCustomDialog();
            }
        });


//Editext

        textSmsWetsilt=findViewById(R.id.smsWetsilt);
        textSmsDrysilt=findViewById(R.id.smsDrysilt);
        textSmsMediumsilt=findViewById(R.id.smsMediumsilt);
        batLowtextsilt=findViewById(R.id.batteryLowsilt);
        textHighsilt=findViewById(R.id.batteryHighsilt);
        //Button
        battLowsilt =findViewById(R.id.batLowThersholdUpdatesilt);
        bttnHighsilt=findViewById(R.id.batHighThersholdUpdatesilt);
        bttsmsDrysilt=findViewById(R.id.dryThersholdUpdatesilt);
        bttnsmsWetsilt=findViewById(R.id.wetThersholdUpdatesilt);
        bttsmsMediumsilt=findViewById(R.id.smsMediumUpdatesilt);
        btnhelp=findViewById(R.id.help_button);

        //gatt  service
        magattService=Write.gattService;

        if(magattService==null){

            Toast.makeText(this,"Device Has benn disconnected ",Toast.LENGTH_LONG).show();
            Intent i= new Intent(this, BrowserActivity.class);
            startActivity(i);
        }



        //validation For all the Fileds

        //==================================================================
        // Button Services

        //==========================LowLevel BAttery ========================

        battLowsilt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value = batLowtextsilt.getText().toString();
                if (value.length() <= 4 && !(value.length()==0)) {
                    if (Integer.valueOf(value) <= 4095) {

                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.batLowThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.batLowThershold.setValue(newValue);
                        batLowtextsilt.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.batLowThershold.setValue(newValue1);
                                magattService.writeCharacteristic(Write.batLowThershold);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.batLowThershold.setValue(value);
                            magattService.writeCharacteristic(Write.batLowThershold);
                        }
                        Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();

                    }
                    else {
                        batLowtextsilt.setError("Enter valid Thershold");
                    }
                }
                else {
                    batLowtextsilt.setError("Enter 4 digit Number ");
                }
            }
        });





        //=====================================================
        //==========================HighLevel BAttery ========================

        bttnHighsilt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = textHighsilt.getText().toString();
                if (value.length() <= 4 && !(value.length() == 0)) {
                    if (Integer.valueOf(value) <= 4095) {

                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.batHighThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.batHighThershold.setValue(newValue);
                        textHighsilt.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.batHighThershold.setValue(newValue1);
                                magattService.writeCharacteristic(Write.batHighThershold);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.batHighThershold.setValue(value);
                            magattService.writeCharacteristic(Write.batHighThershold);
                        }
                        Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                    }
                    else {
                        textHighsilt.setError("Enter valid Thershold");
                    }
                }
                else { textHighsilt.setError("Enter 4 digit Number ");
                }}
        });
        //=====================================================


        //==========================  SMS WET  ========================

        bttnsmsWetsilt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value= textSmsWetsilt.getText().toString();


                if(value.length()<=4 && !(value.length()==0)) {

                    if (Integer.valueOf(value) <= 4095) {


                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.smsWetThershold.setValue(newValue);
                        textSmsWetsilt.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.smsWetThershold.setValue(newValue1);
                                magattService.writeCharacteristic(Write.smsWetThershold);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.smsWetThershold.setValue(value);
                            magattService.writeCharacteristic(Write.smsWetThershold);
                        }
                        Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                    }
                    else {
                        textSmsWetsilt.setError("Enter valid Thershold");
                    }
                }

                else
                {
                    textSmsWetsilt.setError("Enter 4 digit number");
                }
            }

        });
        //=====================================================

        //========================== Wet Dry  ========================

        bttsmsDrysilt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = textSmsDrysilt.getText().toString();
                System.out.println(value.length());
                if (value.length() <= 4 && !(value.length()==0)) {
                    if (Integer.valueOf(value) <= 4095) {

                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.smsDryThershold.setValue(newValue);
                        textSmsDrysilt.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.smsDryThershold.setValue(newValue1);
                                magattService.writeCharacteristic(Write.smsDryThershold);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.smsDryThershold.setValue(value);
                            magattService.writeCharacteristic(Write.smsDryThershold);
                        }
                        Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(),"Updated Successfully",Toast.LENGTH_LONG).show();
                    }
                    else {
                        textSmsDrysilt.setError("Enter valid Thershold");
                    }
                }
                else {
                    textSmsDrysilt.setError("Enter 4 digit number");
                }

            }
        });
        //=====================================================
        //========================== Sms Medium  ========================

        bttsmsMediumsilt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = textSmsMediumsilt.getText().toString();
                System.out.println(value.length());
                if (value.length() <= 4 && !(value.length()==0)) {
                    if (Integer.valueOf(value) <= 4095) {

                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.smsMediumThershold.setValue(newValue);
                        textSmsMediumsilt.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.smsMediumThershold.setValue(newValue1);
                                magattService.writeCharacteristic(Write.smsMediumThershold);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.smsMediumThershold.setValue(value);
                            magattService.writeCharacteristic(Write.smsMediumThershold);
                        }
                        Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(),"Updated Successfully",Toast.LENGTH_LONG).show();
                    }
                    else {
                        textSmsMediumsilt.setError("Enter valid Thershold");
                    }
                }
                else {
                    textSmsMediumsilt.setError("Enter 4 digit number");
                }

            }
        });
        //=====================================================



    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic charac){

        //check mBluetoothGatt is available
        if (magattService == null) {
            Log.e("", "lost connection");
            return false;
        }
//        if (Service == null) {
//            Log.e("", "service not found!");
//            return false;
//        }
        if (charac == null) {
            Log.e("", "char not found!");
            return false;
        }

        boolean status = magattService.writeCharacteristic(charac);
        return status;
    }

    public byte[] hexToByteArray(String hex) {

        if (hex.length() != 0 && hex.length() % 2 != 0) {
            hex = "0" + hex;
        }

        int len =hex.length();

        byte[] byteArr = new byte[len];
        for (int i = 0; i < byteArr.length; i++) {
            int init = i * 2;
            int end = init + 2;
            int temp = Integer.parseInt(hex.substring(init, end), 16);
            byteArr[i] = (byte) (temp & 0xFF);
        }
        return byteArr;
    }
    // Converts string given in decimal system to byte array
    private byte[] decToByteArray(String dec) {
        if (dec.length() == 0) {
            return new byte[]{};
        }

        String data[] = new String[4];
        for (int i = 0;i < dec.length(); i++){
            data[i]= String.valueOf(dec.charAt(i));
        }
        byte[] byteArr = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            try {
                byteArr[i] = (byte) (Integer.parseInt(data[i]));
            } catch (NumberFormatException e) {
                return new byte[]{0};
            }
        }
        return byteArr;
    }
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_help, viewGroup, false);



        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        private Device connectedDevice;

        // Called when device has changed connection status and appropriate
        // broadcast with device address extra is sent
        // It can be either connected or disconnected state
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(true);
                    connectedDevice = device;
                    Intent updateIntent = new Intent(ACTION_GATT_CONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    sendBroadcast(updateIntent);
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(false);
                    Intent updateIntent = new Intent(ACTION_GATT_DISCONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    if (device.equals(connectedDevice)) {
                        connectedDevice = null;
                    }
                    sendBroadcast(updateIntent);
                }
            } else {
                Device device = Engine.getInstance().getDevice(gatt);
                Intent updateIntent = new Intent(ACTION_GATT_CONNECTION_STATE_ERROR);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);
            }
            Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);

        }};

}