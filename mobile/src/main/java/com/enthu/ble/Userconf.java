package com.enthu.ble;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Userconf extends AppCompatActivity {

    public void Sandy(View v) {

        Intent i = new Intent();
        i.setClass(this, Sandy.class);
        startActivity(i);
    }

    public void Clay(View v) {

        Intent i = new Intent();
        i.setClass(this, Clay.class);
        startActivity(i);
    }

    public void Silt(View v) {

        Intent i = new Intent();
        i.setClass(this, Silt.class);
        startActivity(i);
    }

    public void Gendral(View v) {

        Intent i = new Intent();
        i.setClass(this, DatapushActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userconf);



    }
}