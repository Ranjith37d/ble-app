package com.enthu.ble;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.enthu.ble.services.Write;

import java.util.Arrays;

import butterknife.InjectView;

public class Dropdown extends AppCompatActivity {
Spinner spinner_d;
    @InjectView(R.id.help_button)
    TextView helpButton;

    private Dialog helpDialog;
    private Dialog hiddenDebugDialog;
    private String DEVICE_ADDRESS;
    private Dialog alertDialogView;

String type[]={"SANDY","CLAY","SILT"};
EditText SandymediumEdit,SandywetEdit,SandydryEdit,ClaywetEdit,ClaymediumEdit,ClaydryEdit,SiltwetEdit,SiltdryEdit,SiltmediumEdit;
Button sandywet,sandymedium,sandydry,claywet,claydry,claymedium,siltwet,siltdry,siltmedium;
    BluetoothGatt gattService;
 ArrayAdapter<String>arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropdown);

        findViewById(R.id.buttonShowDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //calling this method to show our android custom alert dialog
                showCustomDialog();
            }
        });


        //===============================================
        gattService= Write.gattService;

        spinner_d=(Spinner)findViewById(R.id.spinner_dropdown);
        arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,type);
        spinner_d.setAdapter(arrayAdapter);
//        RelativeLayout batteryLayout = findViewById(R.id.batteryContainer);
//        batteryLayout.setVisibility(View.INVISIBLE);


        //Write BluetoothGatt Service



        spinner_d.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),"your Selection:"+type[i],Toast.LENGTH_SHORT).show();
                Write.colorTypes=type[i];


                LinearLayout layout = findViewById(R.id.container);
                LinearLayout layoutMedium = findViewById(R.id.container1);
                LinearLayout layoutDry = findViewById(R.id.container2);

                LinearLayout layoutbtn = findViewById(R.id.btnwet);
                LinearLayout layoutMediumbtn = findViewById(R.id.btnmedium);
                LinearLayout layoutDrybtn = findViewById(R.id.btndry);

                layout.setVisibility(View.INVISIBLE);
                layoutMedium.setVisibility(View.INVISIBLE);
                layoutDry.setVisibility(View.INVISIBLE);

                layoutbtn.setVisibility(View.INVISIBLE);
                layoutMediumbtn.setVisibility(View.INVISIBLE);
                layoutDrybtn.setVisibility(View.INVISIBLE);

                layout.removeAllViews();
                layoutMedium.removeAllViews();
                layoutDry.removeAllViews();

                layoutbtn.removeAllViews();
                layoutMediumbtn.removeAllViews();
                layoutDrybtn.removeAllViews();



                LinearLayout.LayoutParams wetbtn = new LinearLayout.LayoutParams(190,95);
                wetbtn.setMarginEnd(100);
                wetbtn.gravity = Gravity.RIGHT;

                LinearLayout.LayoutParams mediumbtn = new LinearLayout.LayoutParams(190,95);
                mediumbtn.gravity = Gravity.RIGHT;
                mediumbtn.setMarginEnd(100);

                LinearLayout.LayoutParams drybtn = new LinearLayout.LayoutParams(190,95);
                drybtn.gravity = Gravity.RIGHT;
                drybtn.setMarginEnd(100);


                if("SANDY"==type[i]){

                    LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(650, LinearLayout.LayoutParams.WRAP_CONTENT);

                    lp1.setLayoutDirection(LinearLayout.VERTICAL);
                    lp1.setMarginStart(50);

                    SandywetEdit= new EditText(getApplicationContext());
                    SandymediumEdit= new EditText(getApplicationContext());
                    SandydryEdit= new EditText(getApplicationContext());


                    //=============================Global Value For all activity ===========================
                  Write.sandyWet =SandywetEdit.getText().toString();
                  Write.sandyMedium =SandymediumEdit.getText().toString();
                  Write.sandyDry =SandydryEdit.getText().toString();


                    SandywetEdit.setHint("Wet : 0 - 246 ");
                    SandymediumEdit.setHint("Medium Wet: 246 - 614");
                    SandydryEdit.setHint("Medium Dry : 614 - 1229  ");



                    sandywet= new Button(getApplicationContext());
                    sandywet.setBackgroundColor(Color.parseColor("#00ccff"));
                    sandywet.setTextColor(Color.WHITE);
                    sandymedium= new Button(getApplicationContext());
                    sandymedium.setBackgroundColor(Color.parseColor("#32CD32"));
                    sandymedium.setTextColor(Color.WHITE);
                    sandydry= new Button(getApplicationContext());
                    sandydry.setBackgroundColor(Color.parseColor("#ffff66"));


//                    sandywet.setGravity(Gravity.RIGHT);
//                    sandymedium.setGravity(Gravity.RIGHT);
//                    sandydry.setGravity(Gravity.RIGHT);


                    sandywet.setLayoutParams(wetbtn);
                    sandymedium.setLayoutParams(mediumbtn);
                    sandydry.setLayoutParams(drybtn);


                    sandywet.setText("send");
                    sandymedium.setText("send");
                    sandydry.setText("send");



                    SandywetEdit.setLayoutParams(lp1);
                    SandymediumEdit.setLayoutParams(lp1);
                    SandydryEdit.setLayoutParams(lp1);

                    layout.addView(SandywetEdit);
                    layoutMedium.addView(SandymediumEdit);
                    layoutDry.addView(SandydryEdit);

                    layoutbtn.addView(sandywet);
                    layoutMediumbtn.addView(sandymedium);
                    layoutDrybtn.addView(sandydry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);
                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
                    layoutDrybtn.setVisibility(View.VISIBLE);

                    //========================== Button action For Write CharcterStics ========================
                        //==================== sandy wet ====================
                    sandywet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SandywetEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 246) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandywetEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== sandy Medium ====================
                    sandymedium.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SandymediumEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 614) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandymediumEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== sandy Dry ====================
                    sandydry.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SandydryEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 1024) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SandydryEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==========================================================================//

                }
                else  if("CLAY"==type[i]){



                    LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(600, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp2.setLayoutDirection(LinearLayout.VERTICAL);
                    lp2.setMarginStart(50);



                    ClaywetEdit= new EditText(getApplicationContext());
                    ClaymediumEdit= new EditText(getApplicationContext());
                    ClaydryEdit= new EditText(getApplicationContext());

                    //============================================global value =======================
                    Write.clayWet=ClaywetEdit.getText().toString();
                    Write.clayMedium=ClaymediumEdit.getText().toString();
                    Write.clayDry= ClaydryEdit.getText().toString();


                    ClaywetEdit.setHint("Wet : 0 to 491");
                    ClaymediumEdit.setHint("Medium Wet : 491 - 983 ");
                    ClaydryEdit.setHint("Medium Dry : 983 - 1638  ");



                    claywet= new Button(getApplicationContext());
                    claywet.setBackgroundColor(Color.parseColor("#00ccff"));
                    claywet.setTextColor(Color.WHITE);
                    claymedium= new Button(getApplicationContext());
                    claymedium.setBackgroundColor(Color.parseColor("#32CD32"));
                    claymedium.setTextColor(Color.WHITE);
                    claydry= new Button(getApplicationContext());
                    claydry.setBackgroundColor(Color.parseColor("#ffff66"));

                    claywet.setLayoutParams(wetbtn);
                    claydry.setLayoutParams(mediumbtn);
                    claymedium.setLayoutParams(drybtn);


                    claydry.setText("send");
                    claymedium.setText("send");
                    claywet.setText("send");


                    ClaywetEdit.setLayoutParams(lp2);
                    ClaymediumEdit.setLayoutParams(lp2);
                    ClaydryEdit.setLayoutParams(lp2);

                    layout.addView(ClaywetEdit);
                    layoutMedium.addView(ClaymediumEdit);
                    layoutDry.addView(ClaydryEdit);

                    layoutbtn.addView(claywet);
                    layoutMediumbtn.addView(claymedium);
                    layoutDrybtn.addView(claydry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);
                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
                    layoutDrybtn.setVisibility(View.VISIBLE);

                    //========================== Button action For Write CharcterStics ========================
                    //==================== clay wet ====================
                    claywet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= ClaywetEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 491) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaywetEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== clay Medium ====================
                    claymedium.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= ClaymediumEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 983) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaymediumEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== clay dry  ====================
                    claydry.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= ClaydryEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 1638) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    ClaydryEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });
                    //==========================================================================//

                }
                else {
                    LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(600, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp3.setLayoutDirection(LinearLayout.VERTICAL);
                    lp3.setMarginStart(50);



                    SiltwetEdit= new EditText(getApplicationContext());
                    SiltmediumEdit= new EditText(getApplicationContext());
                    SiltdryEdit= new EditText(getApplicationContext());

                    SiltwetEdit.setHint("Wet : 0 - 1024");
                    SiltmediumEdit.setHint("Medium Wet: 1024 - 1638");
                    SiltdryEdit.setHint("Medium Dry : 1638 - 2048");


                    //=============================global value ==========================================
                    Write.siltWet=SiltwetEdit.getText().toString();
                    Write.siltMedium=SiltmediumEdit.getText().toString();
                    Write.siltDry=SiltdryEdit.getText().toString();

                    siltwet= new Button(getApplicationContext());
                    siltwet.setBackgroundColor(Color.parseColor("#00ccff"));
                    siltwet.setTextColor(Color.WHITE);
                    siltmedium= new Button(getApplicationContext());
                    siltmedium.setBackgroundColor(Color.parseColor("#32CD32"));
                    siltmedium.setTextColor(Color.WHITE);
                    siltdry= new Button(getApplicationContext());
                    siltdry.setBackgroundColor(Color.parseColor("#ffff66"));


                    siltwet.setLayoutParams(wetbtn);
                    siltmedium.setLayoutParams(mediumbtn);
                    siltdry.setLayoutParams(drybtn);


                    siltwet.setText("send");
                    siltmedium.setText("send");
                    siltdry.setText("send");


                    SiltwetEdit.setLayoutParams(lp3);
                    SiltdryEdit.setLayoutParams(lp3);
                    SiltmediumEdit.setLayoutParams(lp3);


                    layout.addView(SiltwetEdit);
                    layoutMedium.addView(SiltmediumEdit);
                    layoutDry.addView(SiltdryEdit);

                    layoutbtn.addView(siltwet);
                    layoutMediumbtn.addView(siltmedium);
                    layoutDrybtn.addView(siltdry);

                    layout.setVisibility(View.VISIBLE);
                    layoutMedium.setVisibility(View.VISIBLE);
                    layoutDry.setVisibility(View.VISIBLE);
                    layoutbtn.setVisibility(View.VISIBLE);
                    layoutMediumbtn.setVisibility(View.VISIBLE);
                    layoutDrybtn.setVisibility(View.VISIBLE);

                    //========================== Button action For Write CharcterStics ========================
                    //==================== silt wet ====================
                    siltwet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SiltwetEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 1024) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsWetThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsWetThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsWetThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsWetThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsWetThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsWetThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltwetEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== silt Medium ====================
                    siltmedium.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SiltmediumEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 1638) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsMediumThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsMediumThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsMediumThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsMediumThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsMediumThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsMediumThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltmediumEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });

                    //==================== silt dry  ====================
                    siltdry.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            String value= SiltdryEdit.getText().toString();

                            if(value.length()<=4 && !(value.length()==0)) {

                                if (Integer.valueOf(value) <= 2048) {


                                    byte[] newValue = value.getBytes();
                                    System.out.println(newValue);

                                    System.out.println(Arrays.toString(newValue));
                                    Write.smsDryThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                                    Write.smsDryThershold.setValue(newValue);


                                    if (value != null) {

                                        byte[] newValue1 = value.getBytes();

                                        try {
                                            Write.smsDryThershold.setValue(newValue1);
                                            gattService.writeCharacteristic(Write.smsDryThershold);

                                        } catch (Exception e) {
                                            Log.e("Service", "null" + e);
                                        }
                                    } else {
                                        Write.smsDryThershold.setValue(value);
                                        gattService.writeCharacteristic(Write.smsDryThershold);
                                    }
                                    Toast.makeText(getApplicationContext(),"Please Wait.......",Toast.LENGTH_LONG).show();
                                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SiltdryEdit.setError("Enter valid Thershold");
                                }
                            }
                        }
                    });
                //====================================================//
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_help, viewGroup, false);



        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }


}