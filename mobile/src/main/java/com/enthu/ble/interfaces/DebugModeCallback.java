package com.enthu.ble.interfaces;

import com.enthu.ble.ble.BluetoothDeviceInfo;

public interface DebugModeCallback {

    void connectToDevice(BluetoothDeviceInfo device);

    void addToFavorite(String deviceAddress);

    void removeFromFavorite(String deviceAddress);

    void addToTemporaryFavorites(String deviceAddress);

    void updateCountOfConnectedDevices();
}
